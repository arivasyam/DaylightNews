package com.example.daylightnews.utils

class Constants {
    companion object{
        const val NEWS_API_KEY = "d027ebb18d704a6aa66bc532a54ff950"
        const val BASE_URL = "https://newsapi.org"
        const val PAGE_SIZE = 20

    }
}